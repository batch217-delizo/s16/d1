console.log("Hello World");


// Section Arithmetic Operator
// + , - , * , /

let x = 1397;
let y = 7831 ;

let add = x + y;
console.log("Result od product operator: " + add);
let difference = x - y;
console.log("Result od quatient operator: " + difference);

let product = x * y;
console.log("Result od product operator: " + product);
let quotient = x / y;
console.log("Result od quatient operator: " + quotient);

let assignment = 8;

// Additional Assignment
let assignmentNumber = assignment + 2;
console.log("Result of assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of assignment operator: " + assignmentNumber);



let mdas = 1 + 2 - 3 *  4 /  5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


// incrementation and decreentation

let z = 1;
// increment
//pre-decrementation
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of post-increment: " + z);
// post-decrementation
increment = z++;
console.log("Result of post-increment " + increment);

// decrement
//pre-decrementation
let decrement = --z;
console.log("Result of pre-increment " + decrement);
console.log("Result of pre-increment " + z);

// post-decrementation
decrement = z--;
console.log("Result of post-increment " + decrement);



// Type Coercion
// Type coercion is the automatic or implicit conversion of values from one data type to another
 
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);


let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);


// section Comparison Operators
let juan = "juan";

// Equality Operator
// = assignment of value
//  == Comparison Equality

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan );

//IneQuality Operator (!=)
// ! = not

console.log(1 != 1);
console.log(1 != 2);


//Strict Equlity Operator (===)
/*

  -Checks if the value or operand are equal are of the same type

*/


console.log(1 === '1');
console.log('juan' === juan);
console.log(0 === false);


// Strict Inequality Operator

console.log(1 !== '1');
console.log('juan' !== juan);
console.log(0 !== false);


//Section Relational Inequality Operator

/*
  -Some comparison operators check whether one value is greater or less then to the other value.
  >,  <, =
*/

let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);


let isLessThan = a >  b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);


// section Logical Operators
// && --> AND, || --> OR, ! --> NOT

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: " + allRequirementsMet);

let someRequirementsMet = !isRegistered;
console.log("Logical && Result: " + allRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Logical && Result: " + someRequirementsNotMet);

